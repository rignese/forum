package com.estm.forum.controller;

import com.estm.forum.dto.CommentsDto;
import com.estm.forum.service.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/api/comments")
@AllArgsConstructor
public class CommentController {

    private final CommentService commentService;

    @PostMapping
    public ResponseEntity<Void> createComment(@RequestBody CommentsDto commentsDto) {
        commentService.save(commentsDto);
        return new ResponseEntity<>(CREATED);
    }

    @GetMapping("/par-sujet/{sujetId}")
    public ResponseEntity<List<CommentsDto>> getAllCommentsForSujet(@PathVariable Long sujetId) {
        return ResponseEntity.status(OK)
                .body(commentService.getAllCommentsForSujet(sujetId));
    }

    @GetMapping("/par-user/{userName}")
    public ResponseEntity<List<CommentsDto>> getAllCommentsForUser(@PathVariable String userName){
        return ResponseEntity.status(OK)
                .body(commentService.getAllCommentsForUser(userName));
    }
}
