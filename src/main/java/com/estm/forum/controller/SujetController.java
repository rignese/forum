package com.estm.forum.controller;

import com.estm.forum.dto.SujetRequest;
import com.estm.forum.dto.SujetResponse;
import com.estm.forum.service.SujetService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/api/sujets")
@AllArgsConstructor
public class SujetController {

    private final SujetService sujetService;

    @PostMapping
    public ResponseEntity<Void> createSujet(@RequestBody SujetRequest sujetRequest) {
        sujetService.save(sujetRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<SujetResponse>> getAllSujets() {
        return status(HttpStatus.OK).body(sujetService.getAllSujets());
    }

    @GetMapping("/{id}")
    public ResponseEntity<SujetResponse> getSujet(@PathVariable Long id) {
        return status(HttpStatus.OK).body(sujetService.getSujet(id));
    }

    @GetMapping("par-theme/{id}")
    public ResponseEntity<List<SujetResponse>> getSujetsByTheme(Long id) {
        return status(HttpStatus.OK).body(sujetService.getSujetsByTheme(id));
    }

    @GetMapping("par-user/{name}")
    public ResponseEntity<List<SujetResponse>> getSujetsByUsername(@PathVariable String name) {
        return status(HttpStatus.OK).body(sujetService.getSujetsByUsername(name));
    }
}
