package com.estm.forum.controller;

import com.estm.forum.dto.ThemeDto;
import com.estm.forum.service.ThemeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/themes")
@AllArgsConstructor
@Slf4j
public class ThemeController {

    private final ThemeService themeService;

    @PostMapping
    public ResponseEntity<ThemeDto> createTheme(@RequestBody ThemeDto themeDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(themeService.save(themeDto));
    }

    @GetMapping
    public ResponseEntity<List<ThemeDto>> getAllThemes() {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(themeService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ThemeDto> getTheme(@PathVariable Long id) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(themeService.getTheme(id));
    }
}
