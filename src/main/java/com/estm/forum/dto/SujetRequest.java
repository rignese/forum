package com.estm.forum.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SujetRequest {
    private Long sujetId;
    private String themeName;
    private String sujetName;
    private String url;
    private String description;
}
