package com.estm.forum.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class SujetResponse {
    private Long id;
    private String sujetName;
    private String url;
    private String description;
    private String userName;
    private String themeName;
    private Integer voteCount;
    private Integer commentCount;
    private String duration;
    private String createdDate;
    private boolean upVote;
    private boolean downVote;
}
