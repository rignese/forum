package com.estm.forum.exceptions;


public class ForumException extends RuntimeException {
    public ForumException(String exMessage) {
        super(exMessage);
    }
}
