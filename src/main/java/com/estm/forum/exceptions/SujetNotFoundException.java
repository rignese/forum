package com.estm.forum.exceptions;

public class SujetNotFoundException extends RuntimeException {
    public SujetNotFoundException(String message) {
        super(message);
    }
}
