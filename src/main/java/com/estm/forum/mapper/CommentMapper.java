package com.estm.forum.mapper;

import com.estm.forum.dto.CommentsDto;
import com.estm.forum.model.Comment;
import com.estm.forum.model.Sujet;
import com.estm.forum.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CommentMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "text", source = "commentsDto.text")
    @Mapping(target = "createdDate", expression = "java(java.time.Instant.now())")
    @Mapping(target = "sujet", source = "sujet")
    @Mapping(target = "user", source = "user")
    Comment map(CommentsDto commentsDto, Sujet sujet, User user);

    @Mapping(target = "sujetId", expression = "java(comment.getSujet().getSujetId())")
    @Mapping(target = "userName", expression = "java(comment.getUser().getUsername())")
    CommentsDto mapToDto(Comment comment);
}