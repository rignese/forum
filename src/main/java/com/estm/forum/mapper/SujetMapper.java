package com.estm.forum.mapper;

import com.estm.forum.dto.SujetRequest;
import com.estm.forum.dto.SujetResponse;
import com.estm.forum.model.*;
import com.estm.forum.repository.CommentRepository;
import com.estm.forum.repository.VoteRepository;
import com.estm.forum.service.AuthService;
import com.github.marlonlom.utilities.timeago.TimeAgo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.Optional;

import static com.estm.forum.model.VoteType.DOWNVOTE;
import static com.estm.forum.model.VoteType.UPVOTE;

@Mapper(componentModel = "spring")
public abstract class SujetMapper {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private VoteRepository voteRepository;
    @Autowired
    private AuthService authService;


    @Mapping(target = "createdDate", expression = "java(java.time.Instant.now())")
    @Mapping(target = "description", source = "sujetRequest.description")
    @Mapping(target = "theme", source = "theme")
    @Mapping(target = "voteCount", constant = "0")
    @Mapping(target = "user", source = "user")
    public abstract Sujet map(SujetRequest sujetRequest, Theme theme, User user);

    @Mapping(target = "id", source = "sujetId")
    @Mapping(target = "themeName", source = "theme.name")
    @Mapping(target = "userName", source = "user.username")
    @Mapping(target = "commentCount", expression = "java(commentCount(sujet))")
    @Mapping(target = "duration", expression = "java(getDuration(sujet))")
    @Mapping(target = "upVote", expression = "java(isSujetUpVoted(sujet))")
    @Mapping(target = "downVote", expression = "java(isSujetDownVoted(sujet))")
    public abstract SujetResponse mapToDto(Sujet sujet);

    Integer commentCount(Sujet sujet) {
        return commentRepository.findBySujet(sujet).size();
    }

    String getDuration(Sujet sujet) {
        return TimeAgo.using(sujet.getCreatedDate().toEpochMilli());
    }

    boolean isSujetUpVoted(Sujet sujet) {
        return checkVoteType(sujet, UPVOTE);
    }

    boolean isSujetDownVoted(Sujet sujet) {
        return checkVoteType(sujet, DOWNVOTE);
    }

    private boolean checkVoteType(Sujet sujet, VoteType voteType) {
        if (authService.isLoggedIn()) {
            Optional<Vote> voteForSujetByUser =
                    voteRepository.findTopBySujetAndUserOrderByVoteIdDesc(sujet,
                            authService.getCurrentUser());
            return voteForSujetByUser.filter(vote -> vote.getVoteType().equals(voteType))
                    .isPresent();
        }
        return false;
    }
}
