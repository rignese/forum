package com.estm.forum.mapper;

import com.estm.forum.dto.ThemeDto;
import com.estm.forum.model.Sujet;
import com.estm.forum.model.Theme;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ThemeMapper {
    @Mapping(target = "numberOfSujets", expression = "java(mapSujets(theme.getSujets()))")
    ThemeDto mapThemeToDto(Theme theme);

    default Integer mapSujets(List<Sujet> numberOfSujets) {
        return numberOfSujets.size();
    }

    @InheritInverseConfiguration
    @Mapping(target = "sujets", ignore = true)
    Theme mapDtoToTheme(ThemeDto themeDto);
}
