package com.estm.forum.repository;

import com.estm.forum.model.Comment;
import com.estm.forum.model.Sujet;
import com.estm.forum.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findBySujet(Sujet sujet);
    List<Comment> findAllByUser(User user);
}
