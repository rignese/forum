package com.estm.forum.repository;

import com.estm.forum.model.Sujet;
import com.estm.forum.model.Theme;
import com.estm.forum.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public interface SujetRepository extends JpaRepository<Sujet, Long> {
    List<Sujet> findAllByTheme(Theme theme);

    List<Sujet> findByUser(User user);
}
