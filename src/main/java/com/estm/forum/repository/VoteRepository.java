package com.estm.forum.repository;

import com.estm.forum.model.Sujet;
import com.estm.forum.model.User;
import com.estm.forum.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Long> {
    Optional<Vote> findTopBySujetAndUserOrderByVoteIdDesc(Sujet sujet, User currentUser);
}
