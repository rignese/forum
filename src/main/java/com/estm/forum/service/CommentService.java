package com.estm.forum.service;

import com.estm.forum.dto.CommentsDto;
import com.estm.forum.exceptions.SujetNotFoundException;
import com.estm.forum.mapper.CommentMapper;
import com.estm.forum.model.Comment;
import com.estm.forum.model.NotificationEmail;
import com.estm.forum.model.Sujet;
import com.estm.forum.model.User;
import com.estm.forum.repository.CommentRepository;
import com.estm.forum.repository.SujetRepository;
import com.estm.forum.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class CommentService {
    private static final String SUJET_URL = "";
    private final SujetRepository sujetRepository;
    private final UserRepository userRepository;
    private final AuthService authService;
    private final CommentMapper commentMapper;
    private final CommentRepository commentRepository;
    private final MailContentBuilder mailContentBuilder;
    private final MailService mailService;

    public void save(CommentsDto commentsDto) {
        Sujet sujet = sujetRepository.findById(commentsDto.getSujetId())
                .orElseThrow(() -> new SujetNotFoundException(commentsDto.getSujetId().toString()));
        Comment comment = commentMapper.map(commentsDto, sujet, authService.getCurrentUser());
        commentRepository.save(comment);

        String message = mailContentBuilder.build(authService.getCurrentUser().getUsername() + " a posté un commentaire sur ton sujet." + SUJET_URL);
        sendCommentNotification(message, sujet.getUser());
    }

    private void sendCommentNotification(String message, User user) {
        System.out.println("Le message "+message);
        System.out.println("Le user "+user);
        mailService.sendMail(new NotificationEmail(user.getUsername() + " a commenté sur ton sujet", user.getEmail(), message));
    }

    public List<CommentsDto> getAllCommentsForSujet(Long sujetId) {
        Sujet sujet = sujetRepository.findById(sujetId).orElseThrow(() -> new SujetNotFoundException(sujetId.toString()));
        return commentRepository.findBySujet(sujet)
                .stream()
                .map(commentMapper::mapToDto).collect(toList());
    }

    public List<CommentsDto> getAllCommentsForUser(String userName) {
        User user = userRepository.findByUsername(userName)
                .orElseThrow(() -> new UsernameNotFoundException(userName));
        return commentRepository.findAllByUser(user)
                .stream()
                .map(commentMapper::mapToDto)
                .collect(toList());
    }
}