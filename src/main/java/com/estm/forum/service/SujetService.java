package com.estm.forum.service;

import com.estm.forum.dto.SujetRequest;
import com.estm.forum.dto.SujetResponse;
import com.estm.forum.exceptions.SujetNotFoundException;
import com.estm.forum.exceptions.ThemeNotFoundException;
import com.estm.forum.mapper.SujetMapper;
import com.estm.forum.model.Sujet;
import com.estm.forum.model.Theme;
import com.estm.forum.model.User;
import com.estm.forum.repository.SujetRepository;
import com.estm.forum.repository.ThemeRepository;
import com.estm.forum.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class SujetService {

    private final SujetRepository sujetRepository;
    private final ThemeRepository themeRepository;
    private final UserRepository userRepository;
    private final AuthService authService;
    private final SujetMapper sujetMapper;

    public Sujet save(SujetRequest sujetRequest) {
        Theme theme = themeRepository.findByName(sujetRequest.getThemeName())
                .orElseThrow(() -> new ThemeNotFoundException(sujetRequest.getThemeName()));
        User currentUser = authService.getCurrentUser();
        return sujetRepository.save(sujetMapper.map(sujetRequest, theme, currentUser));
    }

    @Transactional(readOnly = true)
    public SujetResponse getSujet(Long id) {
        Sujet sujet = sujetRepository.findById(id)
                .orElseThrow(() -> new SujetNotFoundException(id.toString()));
        return sujetMapper.mapToDto(sujet);
    }

    @Transactional(readOnly = true)
    public List<SujetResponse> getAllSujets() {
        return sujetRepository.findAll()
                .stream()
                .map(sujetMapper::mapToDto)
                .collect(toList());
    }

    @Transactional(readOnly = true)
    public List<SujetResponse> getSujetsByTheme(Long themeId) {
        Theme theme = themeRepository.findById(themeId)
                .orElseThrow(() -> new ThemeNotFoundException(themeId.toString()));
        List<Sujet> sujets = sujetRepository.findAllByTheme(theme);
        return sujets.stream().map(sujetMapper::mapToDto).collect(toList());
    }

    @Transactional(readOnly = true)
    public List<SujetResponse> getSujetsByUsername(String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
        return sujetRepository.findByUser(user)
                .stream()
                .map(sujetMapper::mapToDto)
                .collect(toList());
    }
}

