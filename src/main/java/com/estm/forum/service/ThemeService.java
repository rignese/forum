package com.estm.forum.service;

import com.estm.forum.dto.ThemeDto;
import com.estm.forum.exceptions.ForumException;
import com.estm.forum.mapper.ThemeMapper;
import com.estm.forum.model.Theme;
import com.estm.forum.repository.ThemeRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class ThemeService {
    private final ThemeRepository themeRepository;
    private final ThemeMapper themeMapper;

    @Transactional
    public ThemeDto save(ThemeDto themeDto) {
        Theme save = themeRepository.save(themeMapper.mapDtoToTheme(themeDto));
        themeDto.setId(save.getId());
        return themeDto;
    }

    @Transactional(readOnly = true)
    public List<ThemeDto> getAll() {
        return themeRepository.findAll()
                .stream()
                .map(themeMapper::mapThemeToDto)
                .collect(toList());
    }

    public ThemeDto getTheme(Long id) {
        Theme theme = themeRepository.findById(id)
                .orElseThrow(() -> new ForumException("Pas de theme avec ID - " + id));
        return themeMapper.mapThemeToDto(theme);
    }
}
