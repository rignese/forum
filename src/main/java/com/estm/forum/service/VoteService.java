package com.estm.forum.service;

import com.estm.forum.dto.VoteDto;
import com.estm.forum.exceptions.ForumException;
import com.estm.forum.exceptions.SujetNotFoundException;
import com.estm.forum.model.Sujet;
import com.estm.forum.model.Vote;
import com.estm.forum.repository.SujetRepository;
import com.estm.forum.repository.VoteRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static com.estm.forum.model.VoteType.UPVOTE;

@Service
@AllArgsConstructor
public class VoteService {

    private final VoteRepository voteRepository;
    private final SujetRepository sujetRepository;
    private final AuthService authService;

    @Transactional
    public void vote(VoteDto voteDto) {
        Sujet sujet = sujetRepository.findById(voteDto.getSujetId())
                .orElseThrow(() -> new SujetNotFoundException("Sujet Non trouvé avec ID - " + voteDto.getSujetId()));
        Optional<Vote> voteBySujetAndUser = voteRepository.findTopBySujetAndUserOrderByVoteIdDesc(sujet, authService.getCurrentUser());
        if (voteBySujetAndUser.isPresent() &&
                voteBySujetAndUser.get().getVoteType()
                        .equals(voteDto.getVoteType())) {
            throw new ForumException("Tu as deja "
                    + voteDto.getVoteType() + "' pour ce sujet");
        }
        if (UPVOTE.equals(voteDto.getVoteType())) {
            sujet.setVoteCount(sujet.getVoteCount() + 1);
        } else {
            sujet.setVoteCount(sujet.getVoteCount() - 1);
        }
        voteRepository.save(mapToVote(voteDto, sujet));
        sujetRepository.save(sujet);
    }

    private Vote mapToVote(VoteDto voteDto, Sujet sujet) {
        return Vote.builder()
                .voteType(voteDto.getVoteType())
                .sujet(sujet)
                .user(authService.getCurrentUser())
                .build();
    }
}